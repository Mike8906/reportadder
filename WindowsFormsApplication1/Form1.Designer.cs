﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.repName = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Cbtn = new System.Windows.Forms.Button();
            this.Ubtn = new System.Windows.Forms.Button();
            this.Dbtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nAccounting = new System.Windows.Forms.CheckBox();
            this.Accounting = new System.Windows.Forms.CheckBox();
            this.pOrient = new System.Windows.Forms.RadioButton();
            this.lOrient = new System.Windows.Forms.RadioButton();
            this.tRep = new System.Windows.Forms.CheckBox();
            this.pBound = new System.Windows.Forms.CheckBox();
            this.spBound = new System.Windows.Forms.CheckBox();
            this.empBound = new System.Windows.Forms.CheckBox();
            this.dateBound = new System.Windows.Forms.CheckBox();
            this.Exist_name = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.button4 = new System.Windows.Forms.Button();
            this.repFileName = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // repName
            // 
            this.repName.Location = new System.Drawing.Point(105, 69);
            this.repName.Name = "repName";
            this.repName.Size = new System.Drawing.Size(183, 20);
            this.repName.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(297, 32);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(151, 264);
            this.listBox1.TabIndex = 4;
            // 
            // Cbtn
            // 
            this.Cbtn.Location = new System.Drawing.Point(15, 310);
            this.Cbtn.Name = "Cbtn";
            this.Cbtn.Size = new System.Drawing.Size(90, 23);
            this.Cbtn.TabIndex = 5;
            this.Cbtn.Text = "Create";
            this.Cbtn.UseVisualStyleBackColor = true;
            this.Cbtn.Click += new System.EventHandler(this.Cbtn_Click);
            // 
            // Ubtn
            // 
            this.Ubtn.Location = new System.Drawing.Point(111, 310);
            this.Ubtn.Name = "Ubtn";
            this.Ubtn.Size = new System.Drawing.Size(93, 23);
            this.Ubtn.TabIndex = 6;
            this.Ubtn.Text = "Update";
            this.Ubtn.UseVisualStyleBackColor = true;
            this.Ubtn.Click += new System.EventHandler(this.Ubtn_Click);
            // 
            // Dbtn
            // 
            this.Dbtn.Location = new System.Drawing.Point(210, 309);
            this.Dbtn.Name = "Dbtn";
            this.Dbtn.Size = new System.Drawing.Size(93, 24);
            this.Dbtn.TabIndex = 7;
            this.Dbtn.Text = "Delete";
            this.Dbtn.UseVisualStyleBackColor = true;
            this.Dbtn.Click += new System.EventHandler(this.Dbtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Report Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Report File name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(294, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Report Types";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nAccounting);
            this.groupBox1.Controls.Add(this.Accounting);
            this.groupBox1.Controls.Add(this.pOrient);
            this.groupBox1.Controls.Add(this.lOrient);
            this.groupBox1.Controls.Add(this.tRep);
            this.groupBox1.Controls.Add(this.pBound);
            this.groupBox1.Controls.Add(this.spBound);
            this.groupBox1.Controls.Add(this.empBound);
            this.groupBox1.Controls.Add(this.dateBound);
            this.groupBox1.Location = new System.Drawing.Point(15, 121);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(273, 183);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Report settings";
            // 
            // nAccounting
            // 
            this.nAccounting.AutoSize = true;
            this.nAccounting.Location = new System.Drawing.Point(143, 42);
            this.nAccounting.Name = "nAccounting";
            this.nAccounting.Size = new System.Drawing.Size(100, 17);
            this.nAccounting.TabIndex = 8;
            this.nAccounting.Text = "Not Accounting";
            this.nAccounting.UseVisualStyleBackColor = true;
            // 
            // Accounting
            // 
            this.Accounting.AutoSize = true;
            this.Accounting.Location = new System.Drawing.Point(143, 19);
            this.Accounting.Name = "Accounting";
            this.Accounting.Size = new System.Drawing.Size(80, 17);
            this.Accounting.TabIndex = 7;
            this.Accounting.Text = "Accounting";
            this.Accounting.UseVisualStyleBackColor = true;
            // 
            // pOrient
            // 
            this.pOrient.AutoSize = true;
            this.pOrient.Location = new System.Drawing.Point(7, 156);
            this.pOrient.Name = "pOrient";
            this.pOrient.Size = new System.Drawing.Size(58, 17);
            this.pOrient.TabIndex = 6;
            this.pOrient.TabStop = true;
            this.pOrient.Text = "Portrait";
            this.pOrient.UseVisualStyleBackColor = true;
            // 
            // lOrient
            // 
            this.lOrient.AutoSize = true;
            this.lOrient.Location = new System.Drawing.Point(7, 133);
            this.lOrient.Name = "lOrient";
            this.lOrient.Size = new System.Drawing.Size(78, 17);
            this.lOrient.TabIndex = 5;
            this.lOrient.TabStop = true;
            this.lOrient.Text = "Landscape";
            this.lOrient.UseVisualStyleBackColor = true;
            // 
            // tRep
            // 
            this.tRep.AutoSize = true;
            this.tRep.Location = new System.Drawing.Point(6, 111);
            this.tRep.Name = "tRep";
            this.tRep.Size = new System.Drawing.Size(93, 17);
            this.tRep.TabIndex = 4;
            this.tRep.Text = "Telerik Report";
            this.tRep.UseVisualStyleBackColor = true;
            // 
            // pBound
            // 
            this.pBound.AutoSize = true;
            this.pBound.Location = new System.Drawing.Point(6, 88);
            this.pBound.Name = "pBound";
            this.pBound.Size = new System.Drawing.Size(90, 17);
            this.pBound.TabIndex = 3;
            this.pBound.Text = "Period Bound";
            this.pBound.UseVisualStyleBackColor = true;
            // 
            // spBound
            // 
            this.spBound.AutoSize = true;
            this.spBound.Location = new System.Drawing.Point(6, 65);
            this.spBound.Name = "spBound";
            this.spBound.Size = new System.Drawing.Size(122, 17);
            this.spBound.TabIndex = 2;
            this.spBound.Text = "Sales Person Bound";
            this.spBound.UseVisualStyleBackColor = true;
            // 
            // empBound
            // 
            this.empBound.AutoSize = true;
            this.empBound.Location = new System.Drawing.Point(6, 42);
            this.empBound.Name = "empBound";
            this.empBound.Size = new System.Drawing.Size(106, 17);
            this.empBound.TabIndex = 1;
            this.empBound.Text = "Employee Bound";
            this.empBound.UseVisualStyleBackColor = true;
            // 
            // dateBound
            // 
            this.dateBound.AutoSize = true;
            this.dateBound.Location = new System.Drawing.Point(6, 19);
            this.dateBound.Name = "dateBound";
            this.dateBound.Size = new System.Drawing.Size(83, 17);
            this.dateBound.TabIndex = 0;
            this.dateBound.Text = "Date Bound";
            this.dateBound.UseVisualStyleBackColor = true;
            // 
            // Exist_name
            // 
            this.Exist_name.AutoSize = true;
            this.Exist_name.Location = new System.Drawing.Point(12, 38);
            this.Exist_name.Name = "Exist_name";
            this.Exist_name.Size = new System.Drawing.Size(70, 13);
            this.Exist_name.TabIndex = 14;
            this.Exist_name.Text = "Report Name";
            // 
            // comboBox1
            // 
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(105, 35);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(183, 21);
            this.comboBox1.TabIndex = 15;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Checked = true;
            this.radioButton3.Location = new System.Drawing.Point(12, 12);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(47, 17);
            this.radioButton3.TabIndex = 16;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "New";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(67, 12);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(61, 17);
            this.radioButton4.TabIndex = 17;
            this.radioButton4.Text = "Existing";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(234, 96);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(53, 23);
            this.button4.TabIndex = 18;
            this.button4.Text = "Browse";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // repFileName
            // 
            this.repFileName.Location = new System.Drawing.Point(104, 95);
            this.repFileName.Name = "repFileName";
            this.repFileName.Size = new System.Drawing.Size(124, 20);
            this.repFileName.TabIndex = 19;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(367, 310);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 20;
            this.button5.Text = "con settings";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 341);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.repFileName);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.radioButton4);
            this.Controls.Add(this.radioButton3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.Exist_name);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dbtn);
            this.Controls.Add(this.Ubtn);
            this.Controls.Add(this.Cbtn);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.repName);
            this.Name = "Form1";
            this.Text = "Report Adder";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox repName;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button Cbtn;
        private System.Windows.Forms.Button Ubtn;
        private System.Windows.Forms.Button Dbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox dateBound;
        private System.Windows.Forms.CheckBox nAccounting;
        private System.Windows.Forms.CheckBox Accounting;
        private System.Windows.Forms.RadioButton pOrient;
        private System.Windows.Forms.RadioButton lOrient;
        private System.Windows.Forms.CheckBox tRep;
        private System.Windows.Forms.CheckBox pBound;
        private System.Windows.Forms.CheckBox spBound;
        private System.Windows.Forms.CheckBox empBound;
        private System.Windows.Forms.Label Exist_name;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox repFileName;
        private System.Windows.Forms.Button button5;
    }
}

