﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    
    public partial class Form1 : Form
    {
        bool NewRep;
        SqlConnection myConnection;
        public Form1()
        {
            myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
            InitializeComponent();
            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.SQL_ConString))
            {
                try
                {
                    myConnection.Open();
                    myConnection.Close();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
                using (myConnection)
                {
                    SqlCommand sqlCmd = new SqlCommand("SELECT ReportName from tblReports", myConnection);
                    myConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        comboBox1.Items.Add(sqlReader["ReportName"].ToString());
                    }

                    sqlReader.Close();
                }
                myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
                using (myConnection)
                {
                    SqlCommand sqlCmd = new SqlCommand("SELECT * from tblReportTypes", myConnection);
                    myConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        listBox1.Items.Add(String.Format("[{0}]-{1}", sqlReader["ReportTypeID"].ToString(), sqlReader["ReportType"].ToString()));
                    }

                    sqlReader.Close();
                }
                if (radioButton3.Checked)
                {
                    comboBox1.Enabled = false;
                    Ubtn.Enabled = false;
                    Dbtn.Enabled = false;
                    Cbtn.Enabled = true;
                    NewRep = true;
                }
                else
                {
                    comboBox1.Enabled = true;
                    Ubtn.Enabled = true;
                    Dbtn.Enabled = true;
                    Cbtn.Enabled = false;
                    NewRep = false;
                }
                listBox1.SelectedIndex = 0;
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                comboBox1.Enabled = false;
                Ubtn.Enabled = false;
                Dbtn.Enabled = false;
                Cbtn.Enabled = true;
            }
            else
            {
                comboBox1.Enabled = true;
                Ubtn.Enabled = true;
                Dbtn.Enabled = true;
                Cbtn.Enabled = false;
            }
        }
        private string query;
        private int datebound, orientation, salespersonbound, employeebound, periodbound, notacc, acc, telerik,repType;

        private void Dbtn_Click(object sender, EventArgs e)
        {
            execute(builder(2));
            comboBox1.Items.Clear();
            myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
            using (myConnection)
            {
                SqlCommand sqlCmd = new SqlCommand("SELECT ReportName from tblReports", myConnection);
                myConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    comboBox1.Items.Add(sqlReader["ReportName"].ToString());
                }

                sqlReader.Close();
            }

        }

        private void Ubtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(repName.Text) && !string.IsNullOrWhiteSpace(repFileName.Text))
            {
                execute(builder(1));
                myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
                using (myConnection)
                {
                    SqlCommand sqlCmd = new SqlCommand("SELECT ReportName from tblReports", myConnection);
                    myConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        comboBox1.Items.Add(sqlReader["ReportName"].ToString());
                    }

                    sqlReader.Close();
                }
            }
            else {MessageBox.Show("Please Make sure the filename and the report name ore not empty"); }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            repFileName.Text = System.IO.Path.GetFileNameWithoutExtension(ofd.FileName);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ServerSettings ss = new ServerSettings();
            ss.ShowDialog();
        }

        private string repname, repfname;
        
        private string builder(int type = 0)
        {
            string str = listBox1.SelectedItem.ToString();

            int start = str.IndexOf("[");
            int end = str.IndexOf("]");
            string sql = str.Substring(start + 1, end - start - 1);
            repType = Convert.ToInt32(sql);
            repname = repName.Text;
            repfname = repFileName.Text;
            datebound = Convert.ToInt32(dateBound.Checked);
            if (lOrient.Checked)
            {
                orientation = 2;
            }
            if (pOrient.Checked)
            {
                orientation = 1;
            }
            salespersonbound= Convert.ToInt32(spBound.Checked);
            employeebound= Convert.ToInt32(empBound.Checked);
            notacc= Convert.ToInt32(nAccounting.Checked);
            acc= Convert.ToInt32(Accounting.Checked);
            telerik= Convert.ToInt32(tRep.Checked);
            switch (type)
            {
                case 0:
                    query = string.Format("INSERT INTO tblReports(ReportName, ReportFileName, ReportTypeID , DateBound, Orientation,SalesPersonBound,EmployeeBound,PeriodBound,NotAccounting,Accounting,DateCreate,TelerikReport)VALUES('{0}','{1}',{2},{3},{4},{5},{6},{7},{8},{9},GETDATE(),{10}); DECLARE @ReportID AS INT"
+ " SET @ReportID = (SELECT ReportID FROM dbo.tblReports WHERE ReportFileName = '{1}')"
+ " IF NOT EXISTS(SELECT* FROM dbo.tblReportUserLevel WHERE ReportID = @ReportID)"
+ " INSERT INTO dbo.tblReportUserLevel (UserLevelID, ReportID, DateCreated)"
+ " VALUES(1, @ReportID, GETDATE())", repname, repfname,repType, datebound, orientation, salespersonbound, employeebound, periodbound, notacc, acc, telerik);
                    break;
                case 1:
                    query = string.Format("UPDATE tblReports set ReportName='{0}', ReportFileName='{1}', ReportTypeID={2} , DateBound={3}, Orientation={4},SalesPersonBound={5},EmployeeBound={6},PeriodBound={7},NotAccounting={8},Accounting={9},TelerikReport={10} where ReportName='{11}' ", repname, repfname, repType, datebound, orientation, salespersonbound, employeebound, periodbound, notacc, acc, telerik,comboBox1.SelectedItem.ToString());
                    Debug.WriteLine(query);
                    break;
                case 2:
                    query = string.Format("DECLARE @ReportID AS INT"
+ " SET @ReportID = (SELECT ReportID FROM dbo.tblReports WHERE ReportName = '{0}')"
+ "DELETE FROM tblReports where ReportID=@ReportID;" +
"DELETE FROM tblReportUserLevel where ReportID=@ReportID;",comboBox1.SelectedItem.ToString());
                    Debug.WriteLine(query);
                    break;
                default:
                    query = null;
                    break;
            }
            return query;
        }

        private void execute(string sqlCommand)
        {
            if (!string.IsNullOrWhiteSpace(sqlCommand))
            {
                myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
                using (myConnection)
                {
                    SqlCommand sqlCmd = new SqlCommand(sqlCommand, myConnection);
                    myConnection.Open();
                    try
                    {
                        sqlCmd.ExecuteNonQuery();
                        MessageBox.Show("Complete!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    
                    myConnection.Close();
                }
                
            }
        }


        private void Cbtn_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(repName.Text) && !string.IsNullOrWhiteSpace(repFileName.Text))
            { execute(builder());
                myConnection = new SqlConnection(Properties.Settings.Default.SQL_ConString);
                using (myConnection)
                {
                    SqlCommand sqlCmd = new SqlCommand("SELECT ReportName from tblReports", myConnection);
                    myConnection.Open();
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    while (sqlReader.Read())
                    {
                        comboBox1.Items.Add(sqlReader["ReportName"].ToString());
                    }

                    sqlReader.Close();
                }
            }
            else { MessageBox.Show("Please Make sure the filename and the report name ore not empty"); }
        }
    }   
}
