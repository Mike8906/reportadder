﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class ServerSettings : Form
    {
        string connstring = "";
        public ServerSettings()
        {
            InitializeComponent();
            string myServer = Environment.MachineName;

            DataTable servers = SqlDataSourceEnumerator.Instance.GetDataSources();
            for (int i = 0; i < servers.Rows.Count; i++)
            {
                if (myServer == servers.Rows[i]["ServerName"].ToString()) ///// used to get the servers in the local machine////
                {
                    if ((servers.Rows[i]["InstanceName"] as string) != null)
                        CmbServerName.Items.Add(servers.Rows[i]["ServerName"] + "\\" + servers.Rows[i]["InstanceName"]);
                    else
                        CmbServerName.Items.Add(servers.Rows[i]["ServerName"]);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(
                !string.IsNullOrWhiteSpace(textBox1.Text)|| 
                !string.IsNullOrWhiteSpace(textBox2.Text)|| 
                !string.IsNullOrWhiteSpace(CmbServerName.SelectedItem.ToString()))
            {
                connstring = string.Format("Data Source={0};Initial Catalog=amgauto;Persist Security Info=True;User ID={1};Password={2}", CmbServerName.SelectedItem.ToString(), textBox1.Text, textBox2.Text);
                Debug.WriteLine(connstring);
                Properties.Settings.Default.SQL_ConString = connstring;
                Properties.Settings.Default.Save();
            }
            else { MessageBox.Show("all fields arre required");}
        }
    }
}
